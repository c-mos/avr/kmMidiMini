# kmMidiMini - DYI Platform for MIDI Controllers

## Overview

Meet the kmMidiMini, a MIDI controller and interface that seamlessly bridges the gap between vintage analog charm and cutting-edge digital precision. This compact yet powerful device offers an unparalleled blend of retro tactile control and modern versatility, making it the perfect tool for musicians and creators alike.

<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini.jpg" alt="kmMidiMini Board">

With its MIDI DIN and USB interfaces, ADC inputs, Force Sensing Linear Potentiometer (FSLP) support, and infrared remote compatibility, the kmMidiMini brings your entire music setup together in a single, intuitive package. 

Its 4-knob interface mirrors the hands-on experience of vintage analog instruments, giving you direct, satisfying control over sound parameters while leveraging the precision of the digital world. Explore five distinct parameter banks, switch seamlessly between them, and fine-tune your sound with the same organic feel as classic hardware.

Whether you're dialing in lush filters or precise modulation, you'll feel connected to your sound in a way that transcends modern digital interfaces.

Whether you're a seasoned musician yearning for the tactile joy of vintage gear or a forward-thinking creator looking to push sonic boundaries, the kmMidiMini has you covered. Compact, versatile, and designed with open-source principles, it embodies the best of both worlds, letting you explore the analog heart of music in a digital age. The kmMidiMini isn’t just a tool; it’s a bridge between the past and the future of sound.

<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini-populated.jpg" alt="kmMidiMini-populated"><br> 


## Key Features
- MIDI DIN and USB Interfaces
- USART Communication
- Analog-Digital Conversion (ADC) Inputs
- Support for Force Sensing Linear Potentiometer (FSLP)
- Support for Infrared Receivers
- LED Status Indicators
- Programming ISP and OneWire Debug Interfaces
- 32 Hz Control Change Updates in 14 bit high resolution more for kmMidiMini 4K application

## Documentation
For more detailed information about the kmMidiMini PCB, refer to the [kmMidiMini Reference Manual](https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Documentation/kmMidiMini-Reference-Manual.pdf). See also _Documentation.html file in main repository for Doxygen code documentation.


## Applications
### kmMidiMini 4K
When monted on [kmMidiMini 4Knob Case](https://gitlab.com/c-mos/3d-printing/kmmidimini-case) in this Application of kmMidiMini can send MIDI control messages based on rotation of 4 knobs (linear potentiometers - recommended value 10K to 50K Ohm). You can change 5 banks of the midi controll parameters with Button 1 and Button 2. Pressing Button 1 for 2 seconds stores current configuration as default one. Pressing Button 2 for 2 seconds switches between standard 7-bit MIDI message configuration and 14-bit high resolution (NOTE: not all MIDI devices support 14-bit resolution messages) <br>
<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini-4K.jpg" alt="kmMidiMini-4K"><br> 

### kmMidiMini FSLP
In this Application kmMidiMini mimicks Ribbon Controller and can send MIDI control messages based on position and force pressure applied along the length of Force Sensing Linear Potentiometer. Pressing Button 1 switches Ribbon control message (#22) HOLD / RELEASE modes. Pressing Button2 enables / disables sending Cuttuff control message (#71) based on pressure level.<br>
<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini-FSLP.jpg" alt="kmMidiMini-FSLP"><br> 

Here is the example of device connected externally on MOXF synthesiser <br>
<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini-FSLP-mounted.jpg" alt="kmMidiMini-FSLP-mounted"><br> 


### kmMiniShield MIDI I/O Extension
In standard configuration kmMidiMini works only as MIDI OUT device (for MIDI DIN5 connection). An open hardware MIDI shield designed to facilitate MIDI communication through **MIDI_IN**, **MIDI_OUT**, and **MIDI_THROUGH** ports is available at [kmMiniSchield MIDI I/O PCB Repository](https://gitlab.com/c-mos/pcb/kmMiniShieldMidiIOPCB). 

<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini-MidiIO.jpg" alt="kmMidiMini-MidiIO"><br> 
<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini-MidiIO2.jpg" alt="kmMidiMini-MidiIO2"><br> 

The shield is designed for easy integration with microcontroller-based system  [kmMidiMini PCB](https://gitlab.com/c-mos/pcb/kmMidiMiniPCB) or [ATB-Starter Kit](https://atnel.pl/instrukcja-atb-rev-1-05a.html)
<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/kmMidiMini-IO.jpg" alt="kmMidiMini-IO"><br> 

## Compilation and Installation
### Getting Sources
To get this version use following git commands

```bash
git clone git@gitlab.com:c-mos/avr/kmMidiMini.git
cd kmMidiMini
git submodule update --init
```

### Setting up fuse bits
```bash
avrdude -c usbasp -p m328pb -U lfuse:w:0x77:m -U hfuse:w:0xd9:m -U efuse:w:0xFF:m
```
### Flashing firmware
- Download the appropriate `.hex` file for your AVR device from [kmMidiMiniReleases](https://gitlab.com/c-mos/kmAvrReleases/kmMidiMiniReleases) repository.
- Flash the downloaded `.hex` file onto your AVR device using your preferred programming tool or software.
- For USB-ASP use following avrdude command:
```bash
avrdude -c usbasp -p m328pb -U flash:w:kmMidiMini_vXXXX.hex:i
```

Software has been compiled and tested with [**Atmel Studio 7**](https://ww1.microchip.com/downloads/en/DeviceDoc/installer-7.0.2389-full.exe)

## Editing PCB Projects
<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/logo-kicad.png" alt="Designed with KiCad"><br> 
The PCB Designe project have been prepared with [KiCad 5.1.12](https://downloads.kicad.org/kicad/windows/explore/stable/download/kicad-5.1.12_1-x86_64.exe). Make sure [kmKiCadFootprints](https://gitlab.com/c-mos/pcb/kmKiCadFootprints), [kmKicadSymbols](https://gitlab.com/c-mos/pcb/kmKiCadSymbols), and [DigiKey KiCad Library](https://github.com/Digi-Key/digikey-kicad-library) is installed before opening the design files.

## Getting PCBs and populating electronic components
1. Download the Gerber files provided in the corresponding PCB project repository (refer to the [Components](#components) section for details).  
2. Compress the Gerber files into a ZIP archive.  
3. Upload the ZIP file to the website of your preferred PCB manufacturer and follow their instructions for ordering. (Most PCB makers will guide you through selecting options such as material, layer count, thickness, and solder mask color.)  
4. Place the order and wait for your custom PCBs to be manufactured and shipped.
> **Tip:** Many popular PCB manufacturers, such as JLCPCB, PCBWay, or OSH Park, provide step-by-step guides on their websites for submitting Gerber files.
5. Order the electronic components listed in the kmMidiMini_BOM and kmMiniShieldMidiIO_BOM files for their respective PCB design projects. Once received, ensure that each component is mounted in its correct position on the PCB. Note that most components in the BOM files are already assigned to specific locations. Some components are optional, and their selection depends on the specific application being used. Refer to the 'Instruction/Notes' column in the BOM files for detailed guidance on which components to include.
6. 
<img src="https://gitlab.com/c-mos/avr/kmMidiMini/-/raw/main/Images/logo-pcbw.png" alt="PCBWay+ Open Source Community"><br> 
The PCB project is also available on [PCBWay+ Open Source Community](https://www.pcbway.com/project/member/?bmbno=36A1E75E-4C29-4F):
- [v1.0 kmMidiMini - your DYI platform for MIDI controllers](https://www.pcbway.com/project/shareproject/kmMidiMini_your_DYI_platform_for_MIDI_controllers_1d12e542.html)<br>
<img src="https://gitlab.com/c-mos/pcb/kmMidiMiniPCB/-/raw/main/oshView/01_BoardTop_Transparent.png" alt="kmMidiMini Board" width="800" > <br>
- [v1.0 kmMiniSchield MIDI I/O - IN/OUT/THROUGH MIDI extension for kmMidiMini](https://www.pcbway.com/project/shareproject/kmMiniSchield_MIDI_I_O_IN_OUT_THROUGH_MIDI_extension_for_kmMidiMini_07c4e261.html)
<img src="https://gitlab.com/c-mos/pcb/kmMiniShieldMidiIOPCB/-/raw/main/oshView/01_BoardTop_Transparent.png" alt="kmMidiShield MIDI I/O Board" width="700" > <br>
> Note: Default settings PCB Specification (1.6mm thickness, 2 sides, etc.) should be OK, the Author recommends to set `Surface Finish` option to **`HASL lead free`** and `Remove product No.` option to **`Specify a location`**. Don't forget to pick your favorite color :-)

## Components
- [kmMidiMini PCB](https://gitlab.com/c-mos/pcb/kmMidiMiniPCB)
- [kmMiniSchield MIDI I/O PCB](https://gitlab.com/c-mos/pcb/kmMiniShieldMidiIOPCB)
- [kmMidiMini 4Knob Case](https://gitlab.com/c-mos/3d-printing/kmmidimini-case)
- [kmAvrFramework Libraries](https://gitlab.com/c-mos/kmAvrLibs)
- [www.obdev.at V-USB](https://www.obdev.at/products/vusb/index.html)

## Links and Resources

You can find the design files and detailed schematics on the official repository:

- [Git Repository](https://gitlab.com/c-mos/pcb/kmMidiMiniPCB)
- [YouTube Tutorials EN](https://www.youtube.com/@e-lectronics)
- [YouTube Tutorials PL](https://www.youtube.com/@km-elektronika)

---

## 📝 Note
> **This is an open-source DIY project.** The Author does **not** sell ready-made devices. If you are interested in building the device, follow the provided instructions and source the components.

## Author and License

This project is released under the [**Open Hardware**](https://www.oshwa.org) license. You are free to use, modify, and distribute this design under the terms of the license. Please give proper attribution when sharing or modifying the design.

**Author**: Krzysztof Moskwa

**e-mail**: chris[dot]moskva[at]gmail[dot]com

**General License**: GPL-3.0-or-later

GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)


