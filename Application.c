/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* Application.c
*
*  Created on: 3/15/2024 4:55:07 PM
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Test Application for kmAvrLibs
*  Copyright (C) 2024  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "kmCommon/kmCommon.h"

#include "Application.h"

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"
#include "kmSwtTimer/kmSoftwareTimer.h"
#include "kmUSART/kmUSART.h"
#include "kmTerm/kmTerm.h"
#include "kmLog/kmLog.h"
#include "kmIR/kmIrReceiver.h"
#include "kmIr/maps/kmIrRemotesExample.h"
#include "kmButtons/kmButtons.h"

#include "kmADC/kmAdc.h"
#include "kmADC/kmFslp.h"
#include "kmADC/kmAdcAverager.h"
#include "kmADC/kmAdcTransposeDefs.h"
#include "kmADC/kmDetector.h"
#include "kmADC/kmDetectorExamples.h"
#include "kmTimer0/kmTimer0.h"

#include "kmMidi/kmMidi.h"
#include "kmMidi/kmMidiChords.h"
#include "kmSettingsMidiMini/kmSettingsMidiMini.h"

#include "kmUsb/kmUsb.h"
#include "kmUsb/kmUsbMidi.h"

// "private" structures and definitions
#define APP_STATUS_LED_DEFAULT {	.statusLedType = appStatusLedsOperations, \
									.statusLedOperationsState = KMC_UNSIGNED_ZERO, \
									.statusLedModeState = KMC_UNSIGNED_ZERO, \
									.statusLedModeStep = KMC_UNSIGNED_ZERO \
								}

typedef enum {
	appStatusLedsDisabled,
	appStatusLedsOperations,
	appStatusLedsMode,
} appStatusLedType;

typedef struct {
	appStatusLedType statusLedType;
	uint8_t statusLedOperationsState;
	uint8_t statusLedModeState;
	uint8_t statusLedModeStep;
} appStatusLed;

typedef enum {
	kmIrModeMidiMode,
	kmIrModeMidiChannel,
	kmIrModeMidiChord,
	kmIrModeMidiNote
} appIrMode;

// "private" variables
static uint16_t _appMidiGlobalChannel = KM_MIDI_DEFAULT_CHANNEL;
static uint8_t _appMidiDevice = KM_MIDI_DEFAULT_DEVICE;
static appIrMode _appIrMode = kmIrModeMidiMode;
static appStatusLed _appStatusLed = APP_STATUS_LED_DEFAULT;


kmMidiChordType _exampleChords[] = {
	{
		.baseNote = kmMidiNoteCSharp4,
		.kind = kmMidiChordMinor,
		.incllSubTone = true
	},
	{
		.baseNote = kmMidiNoteE4,
		.kind = kmMidiChordMajor,
		.incllSubTone = true
	},
	{
		.baseNote = kmMidiNoteB4,
		.kind = kmMidiChordMajor,
		.incllSubTone = true
	},
	{
		.baseNote = kmMidiNoteA4,
		.kind = kmMidiChordMajor,
		.incllSubTone = true
	},
	{
		.baseNote = kmMidiNoteFSharp4,
		.kind = kmMidiChordMinor,
		.incllSubTone = true
	},
	{
		.baseNote = kmMidiNoteB4,
		.kind = kmMidiChordMajor,
		.incllSubTone = true
	},
	{
		.baseNote = kmMidiNoteCSharp4,
		.kind = kmMidiChordMajor,
		.incllSubTone = true
	},
};

// "private" functions
void appInitStatusLed(void);
void appCallbackStatusLed(const void *userData, kmSwtIntervalValueType *newTimerValue);
void appCallbackIrLed(const void *userData, kmSwtIntervalValueType *newTimerValue);
void appCallbackMidiLed(const void *userData, kmSwtIntervalValueType *newTimerValue);
void appCallbackButtonsCheck(const void *userData, kmSwtIntervalValueType *newTimerValue);
void appCallbackButtons(const kmButtonType button, const kmButtonStateType buttonState);
void appInitSwtTimer(void);
void appInitUsartDebugTerm(void);
void appInitMidi(void);
void appUsartLoop(void);
void appIrDispatchMidiChannel(kmIrCommand command);
void appIrDispatchMidiNote(kmIrCommand command);
void appIrDispatchMidiChord(kmIrCommand command);
void appIrDispatcher(kmIrCommand command);
void appCallbackIrCapture(const uint32_t data);
void appInitIr(void);
void appButtonsInit(void);
void printAdcValues(void);
void appCallbackAdcAveragingProcecessed(void *userData);
void kmMidiDispatcher(const uint8_t ctrlNo, const uint16_t data);
void appCallbackAdcChannelDetected(const void *userData, const uint8_t detectorNo, const uint16_t value);
void appInitAdc(void);
void appShowInfo(void);
void appSetupChange(const kmSettingsMidiAppSetup setup);

// App setup
void appInitStatusLed(void) {
	dbPullUpAllPorts();
	dbInit();
}

void appStatusLedUpdate(void) {
	switch (_appStatusLed.statusLedType) {
		case appStatusLedsOperations : {
			KM_MM_LED_PORT = (~_appStatusLed.statusLedOperationsState & KM_MM_LED_MASK) | (KM_MM_LED_PORT & ~KM_MM_LED_MASK);
		}
		break;
		case appStatusLedsMode : {
			KM_MM_LED_PORT = (~_appStatusLed.statusLedModeState & KM_MM_LED_MASK) | (KM_MM_LED_PORT & ~KM_MM_LED_MASK);
		}
		break;
		default : {
		// disabled
			KM_MM_LED_PORT |= KM_MM_LED_MASK;
		}
	}
}

void appStatusLedOperationSet(bool on, uint8_t bitPosition) {
	_appStatusLed.statusLedOperationsState = 
			on ?	(_appStatusLed.statusLedOperationsState |  _BV(bitPosition))
				:	(_appStatusLed.statusLedOperationsState & ~_BV(bitPosition));
	appStatusLedUpdate();
}

void appCallbackStatusLed(const void *userData, kmSwtIntervalValueType *newTimerValue) {
	static bool statusOn = false;
	if (true == statusOn) {
		statusOn = false;
		*newTimerValue = KM_STATUS_LED_TIME_OFF;
	} else {
		statusOn = true;
		*newTimerValue = KM_STATUS_LED_TIME_ON;
	}
	appStatusLedOperationSet(statusOn, KM_MM_LED_STATUS);
}

void appIndicateOperationMidi(void) {
	appStatusLedOperationSet(true, KM_MM_LED_MIDI);
	kmSwtStart(KM_SWT_SLOT_MIDI_LED, KM_MIDI_LED_TIME_ON);
}

void appIndicateOperationIr(void) {
	appStatusLedOperationSet(true, KM_MM_LED_IR);
	kmSwtStart(KM_SWT_SLOT_IR_LED, KM_IR_LED_TIME_ON);
}

void appIndicateModeState(uint8_t indication, uint8_t steps) {
	_appStatusLed.statusLedModeState = indication;
	_appStatusLed.statusLedModeStep = steps << KMC_MULT_BY_2;
	_appStatusLed.statusLedType = appStatusLedsMode;

	appStatusLedUpdate();
	kmSwtStart(KM_SWT_SLOT_MODE_LED, APP_MODE_LED_TIME_ON);
}

void appCallbackIrLed(const void *userData, kmSwtIntervalValueType *newTimerValue)  {
	appStatusLedOperationSet(false, KM_MM_LED_IR);
}

void appCallbackMidiLed(const void *userData, kmSwtIntervalValueType *newTimerValue) {
	appStatusLedOperationSet(false, KM_MM_LED_MIDI);
}

void appCallbackModeLed(const void *userData, kmSwtIntervalValueType *newTimerValue) {

	if (_appStatusLed.statusLedModeStep == KMC_UNSIGNED_ZERO) {
		_appStatusLed.statusLedType = appStatusLedsOperations;
		appStatusLedUpdate();
		return;
	}

	_appStatusLed.statusLedModeStep--;
	if (_appStatusLed.statusLedModeStep & KMC_UNSIGNED_ONE) {
		_appStatusLed.statusLedType = appStatusLedsDisabled;
		*newTimerValue = APP_MODE_LED_TIME_OFF;
	} else {
		_appStatusLed.statusLedType = appStatusLedsMode;
		*newTimerValue = APP_MODE_LED_TIME_ON;
	}

	appStatusLedUpdate();
	kmSwtStart(KM_SWT_SLOT_MODE_LED, APP_MODE_LED_TIME_ON);
}

void appModeSet(uint8_t mode) {
	kmSettingsMidiCtrlSetMode(mode);
	appSetupChange(kmSettingsGetAppSetup());
	appIndicateModeState(kmSettingsMidiCtrlGetIndication(), KM_MODE_CHANGE_BLINKS);
}

void appModeChange(bool inc) { 
	kmSettingsMidiCtrlChangeMode(inc);
	appSetupChange(kmSettingsGetAppSetup());
	appIndicateModeState(kmSettingsMidiCtrlGetIndication(), KM_MODE_CHANGE_BLINKS);
}

void appModeSave(void) {
	appIndicateModeState(kmSettingsMidiCtrlGetIndication(), KM_MODE_SAVE_BLINKS);
	kmSettingsMidiCtrlStore();
}

void appResolutionChange(void) {
	bool hiRes = kmSettingsMidiCtrlSetHiRes(!kmSettingsMidiCtrlGetHiRes());
	uint8_t indicationIndex = hiRes ? KMC_UNSIGNED_ONE : KMC_UNSIGNED_ZERO;
	appIndicateModeState(indicationIndex + KM_MODE_INDICATION_MAP_HIRES_OFFSET, KM_MODE_CHANGE_BLINKS);
}

void appCallbackButtonsCheck(const void *userData, kmSwtIntervalValueType *newTimerValue) {
	kmButtonsLoop();
	*newTimerValue = KM_BUTTON_CHECK_INTERVAL;
}

void appCallbackButtons(const kmButtonType button, const kmButtonStateType buttonState) {
	switch (buttonState) {
		case kmButtonPressed : {
			appIndicateModeState(kmSettingsMidiCtrlGetIndication(), KM_MODE_CHANGE_SHOW);
			break;
		}
		case kmButtonReleased : {
			switch (button) {
				case KM_MM_BTN1 : {
					appModeChange(false);
					break;
				}
				case KM_MM_BTN2 : {
					appModeChange(true);
					break;
				}
				default : {
					// intentionally
				}
			}
		break;
		}
		case kmButtonReleasedInRepeat : {
			switch (button) {
				case KM_MM_BTN1 : {
					appModeSave();
					break;
				}
				case KM_MM_BTN2 : {
					appResolutionChange();
					break;
				}
				default : {
					// intentionally
				}
			}
		break;
		}
		default : {
		// intentionally
		}
	}
}

void appInitSwtTimer(void) {
	kmSwtInit(KM_SWT_INTERVAL_10MS);
	kmSwtRegisterCallback(KM_SWT_SLOT_STATUS_LED, KM_SWT_USER_DATA(NULL), appCallbackStatusLed);
	kmSwtStart(KM_SWT_SLOT_STATUS_LED, KM_STATUS_LED_TIME_ON);

	kmSwtRegisterCallback(KM_SWT_SLOT_IR_LED, KM_SWT_USER_DATA(NULL), appCallbackIrLed);
	kmSwtRegisterCallback(KM_SWT_SLOT_MIDI_LED, KM_SWT_USER_DATA(NULL), appCallbackMidiLed);
	kmSwtRegisterCallback(KM_SWT_SLOT_MODE_LED, KM_SWT_USER_DATA(NULL), appCallbackModeLed);
}

// USART (Midi and Terminal)
void appInitUsartDebugTerm(void) {
#ifdef KM_MM_USART_DEBUG
	kmUsartInit(KM_MM_USART_DEBUG, KM_MM_USART_DEBUG_NO, KM_USART_DEBUG_SPEED);
#ifndef KM_LOG_DISABLE
	kmLogAddWriter(KM_MM_USART_DEBUG, kmUsartWrite);
#endif
#endif /* KM_MM_USART_DEBUG */
}

void appCallbackUsbMidiConnected(void) {
	dbOn(KM_MM_DB_PIN_IR);
}

void appInitMidi(void) {
	kmUsartInit(KM_MM_USART_MIDI, KM_MM_USART_MIDI_NO, KM_MM_USART_MIDI_SPEED);
	kmMidiInitWithTerminator(_appMidiDevice, KM_MM_USART_MIDI, kmUsbMidiWriter, kmUsbMidiReader, kmUsbMidiTerminate);
	kmUsbMidiInit(kmUsartWrite, kmUsartRead);
	kmUsbMidiRegisterConnectedCallback(appCallbackUsbMidiConnected);
}

void appUsartLoop(void) {
#ifdef KM_MM_USART_DEBUG
	kmUsartRead(KM_MM_USART_DEBUG);
#endif

#ifdef KM_MM_USART_MIDI
	kmUsartRead(KM_MM_USART_MIDI);
#endif
}

void appIrDispatchMidiMode(kmIrCommand command) {
	switch(command) {
		case kmIrCmd1		: appModeSet(0x00);			break;
		case kmIrCmd2		: appModeSet(0x01);			break;
		case kmIrCmd3		: appModeSet(0x02);			break;
		case kmIrCmd4		: appModeSet(0x03);			break;
		case kmIrCmd5		: appModeSet(0x04);			break;
		case kmIrCmdChUp	: appModeChange(false);		break;
		case kmIrCmdChDown	: appModeChange(true);		break;
		default:										break; // intentionally
	}
}


void appIrDispatchMidiChannel(kmIrCommand command) {
	// 0 = Channel 10
	if (command < kmIrCmd1 || command > kmIrCmd0) return;
	
	_appMidiGlobalChannel = command - kmIrCmd1;
}

void appIrDispatchMidiNote(kmIrCommand command) {
	if (command < kmIrCmd1 || command > kmIrCmd0) return;
	
	static int8_t lastNote = KMC_SIGNED_MINUS_ONE;
	if (KMC_SIGNED_MINUS_ONE != lastNote) {
		kmMidiSendNoteOff(_appMidiDevice, _appMidiGlobalChannel, kmMidiNoteC4 + lastNote, KM_MIDI_DEFAULT_VELOCITY);
	}

	lastNote = command - kmIrCmd1;

	kmMidiSendNoteOn(_appMidiDevice, _appMidiGlobalChannel, kmMidiNoteC4 + lastNote, KM_MIDI_DEFAULT_VELOCITY);
}

void appIrDispatchMidiChord(kmIrCommand command) {
	if (command < kmIrCmd1 || command > kmIrCmd0) return;
	
	static int8_t lastChord = KMC_SIGNED_MINUS_ONE;
	if (KMC_SIGNED_MINUS_ONE != lastChord) {
		kmMidiChordNoteOff(_appMidiDevice, _appMidiGlobalChannel, &_exampleChords[lastChord]);
	}
	lastChord = command - kmIrCmd1;

	kmMidiChordNoteOn(_appMidiDevice, _appMidiGlobalChannel, &_exampleChords[lastChord], KM_MIDI_DEFAULT_VELOCITY);
}

void appIrDispatcher(kmIrCommand command) {
	switch(command) {
		case kmIrCmdRed : {
			_appIrMode = kmIrModeMidiMode;
			break;
		}
		case kmIrCmdGreen : {
			_appIrMode = kmIrModeMidiChord;
			break;
		}
		case kmIrCmdYellow : {
			_appIrMode = kmIrModeMidiNote;
			break;
		}
		case kmIrCmdBlue : {
			_appIrMode = kmIrModeMidiChannel;
			break;
		}
		case kmIrCmdPower : {
			kmSettingsMidiCtrlStore();
			break;
		}
		case kmIrCmdHome : {
			kmSettingsMidiCtrlInitDefaultProfile();
			_appIrMode = kmIrModeMidiChannel;
			_appMidiGlobalChannel = KM_MIDI_DEFAULT_CHANNEL;
			kmMidiSendStatusMessageShort(_appMidiDevice, _appMidiGlobalChannel, kmMidiControlChange, kmMidiCtrlAllSoundOff);
			break;
		}
		default : {
		// intentionally
		}
	}

	switch(_appIrMode) {
		case kmIrModeMidiChannel : {
			appIrDispatchMidiChannel(command);
			break;
		}
		case kmIrModeMidiNote : {
			appIrDispatchMidiNote(command);
			break;
		}
		case kmIrModeMidiChord : {
			appIrDispatchMidiChord(command);
			break;
		}
		default: {
			appIrDispatchMidiMode(command);
		}
	}
}

// Infrared Receiver (IR)
void appCallbackIrCapture(const uint32_t data) {
	if (data < KMC_UNUSED_16_BIT) return;

	appIndicateOperationIr();

	kmIrCommand command = kmIrGetCommandByCode((uint16_t)(data >> KMC_DIV_BY_65536), (uint16_t)data);
#ifndef KM_LOG_DISABLE
	sprintf_P(kmLogMemBuffer, PSTR("name, %08lx, cmd: %02x, name: %s"), data, command);
	KM_LOG_P(KM_LOG_SOURCE, kmLogSeverityDebug, kmLogCodeDebug, kmIrGetCommandName_P(command), kmLogMemBuffer);
#endif
	appIrDispatcher(command);
}

void appInitIr(void) {
	kmIrInit();
	kmIrRegisterCaptureCallback(appCallbackIrCapture);
	
	kmIrSetRemotes_P(kmIrRemoteDefsExample, sizeof(kmIrRemoteDefsExample) / sizeof(kmIrRemote));
	kmIrSetRepeatEnabled(true);
}

void appButtonsInit(void) {
	kmButtonsInit();
	kmButtonsRegisterCallback(appCallbackButtons);

	kmSwtRegisterCallback(KM_SWT_SLOT_BUTTONS, KM_SWT_USER_DATA(NULL), appCallbackButtonsCheck);
	kmSwtStart(KM_SWT_SLOT_BUTTONS, KM_BUTTON_CHECK_INTERVAL);
}

// ADC
#ifdef KM_LOG_ADC_EVERY
void printAdcValues(void) {

	static uint8_t printAdcEverySteps = KM_LOG_ADC_EVERY;

	if (printAdcEverySteps--) {
		return;
	} else {
		printAdcEverySteps = KM_LOG_ADC_EVERY;
	}
	
	uint8_t controlArrayLen = kmAdcGetControlDataArrayLen();
	for (uint8_t i = KMC_UNSIGNED_ZERO; i < controlArrayLen; i++) {
		sprintf_P(kmLogMemBuffer, PSTR("%02x - 0x%04x;\t"), i,  kmAdcAveragerGetValue(i));
		kmTermPrintString(KM_MM_USART_DEBUG, kmLogMemBuffer);
	}
	kmTermPrintLn(KM_MM_USART_DEBUG);
}
#endif

void appCallbackAdcAveragingProcecessed(void *userData) {
#ifndef KM_NO_ADC_DETECTOR
	uint8_t controlArrayLen = KM_ADC_NUMBER_OF_DETECTORS;
	for (uint8_t i = KMC_UNSIGNED_ZERO; i < controlArrayLen; i++) {
		kmDetectorSetValue(i, kmAdcAveragerGetValue(i));
	}
#endif
#ifdef KM_LOG_ADC_EVERY
	printAdcValues();
#endif
}

void kmMidiDispatcher(const uint8_t ctrlNo, const uint16_t data) {
#ifdef KM_MIDI_VERBOSE_LOGS
	sprintf_P(kmLogMemBuffer, PSTR("(ctrlNo:%i, data:%p"), ctrlNo, data);
	KM_LOG_P(KM_LOG_SOURCE, kmLogSeverityDebug, kmLogCodeDebug, PSTR("kmMidiDispatcher"), kmLogMemBuffer);
#endif
	kmSettingsMidiController *instruction = kmSettingsMidiCtrlGet(ctrlNo);

	uint8_t channel = _appMidiGlobalChannel;
	if (-1 != instruction->channel) {
		channel = instruction->channel;
	}

	switch (instruction->voiceMessageType) {
		case kmMidiPitchBend : {
			kmMidiSendStatusMessageLong16(_appMidiDevice, channel, instruction->voiceMessageType,  data);
			break;
		}
		case kmMidiControlChange : {
			kmMidiSendCtrlChangeHires(_appMidiDevice, channel, instruction->controllerType, instruction->hiresMode, data);
			break;
		}
		default : {
			// intentional
		}
	}
}

void appCallbackAdcChannelDetected(const void *userData, const uint8_t detectorNo, const uint16_t value) {
#ifdef KM_LOG_ADC_DETECTOR
	sprintf_P(kmLogMemBuffer, PSTR("DETECTED - detector:0x%02x, value: 0x%04x"), detectorNo, value);
	KM_LOG_P(KM_LOG_SOURCE, kmLogSeverityDebug, kmLogCodeDebug, PSTR("appCallbackAdcChannelDetected"), kmLogMemBuffer);
#endif

	appIndicateOperationMidi();

	const uint16_t ctrlNo = (const uint16_t)userData;
	kmMidiDispatcher(ctrlNo, value);
}

void appSetupChange(const kmSettingsMidiAppSetup setup) {
	#ifndef KM_NO_ADC_DETECTOR
		for (uint8_t i = KMC_UNSIGNED_ZERO; i < KM_ADC_NUMBER_OF_DETECTORS; i++) {
			kmDetectorSetConfig(i, KM_DETECTOR_USER_DATA(kmSettingsGetDetectorMidiCtrl(i)), kmSettingsGetDetectorConfig(i));
		}

		// main detectors callback
		kmDetectorRegisterChannelDetectedCallback(appCallbackAdcChannelDetected);
	#endif

	kmAdcRegisterSlotCompletedCallback(KM_ADC_USER_DATA(NULL), setup == appFSLP ? kmAdcFslpChannelCompletedCallback : NULL);
	
	kmAdcInitOnAccurateTimeWithTimer0(KM_ADC_MAIN_SAMPLING_PERIOD, kmSettingsGetAdcControlDataArray(), kmSettingsGetAdcControlDataLen());
	
	kmAdcSetReference(setup == appFSLP ? KM_ADC_REF_AVCC : KM_ADC_REF_AREF);
}

void appInitAdc(void) {
	KM_MM_ADC_PORT &= ~KM_MM_ADC_MASK;
	KM_MM_GPIO_PORT &= ~KM_MM_GPIO_MASK;

	kmAdcRegisterAveragerCallback(KM_ADC_AVERAGER_USER_DATA(NULL), appCallbackAdcAveragingProcecessed);
	kmAdcRegisterSamplingCompletedCallback(KM_ADC_USER_DATA(NULL), kmAdcAveragerSamplingProcessedRoutine);

	appSetupChange(kmSettingsGetAppSetup());
}

void appShowInfo(void) {
	kmTermResetAttributes(KM_MM_USART_DEBUG);
	kmTermClearScreen(KM_MM_USART_DEBUG);
	kmTermCursorHome(KM_MM_USART_DEBUG);
	kmTermPrintLnString_P(KM_MM_USART_DEBUG, KM_STR_APP_NAME_VERSION);
	kmTermPrintLnString_P(KM_MM_USART_DEBUG, KM_STR_APP_COPYRIGHT);
	kmTermPrintLnString_P(KM_MM_USART_DEBUG, KM_STR_APP_REPO);
}

// "public" functions
void appInit(void) {
	appInitStatusLed();
	kmSettingsMidiCtrlInit();

	appInitMidi();
	appInitSwtTimer();
	appInitUsartDebugTerm();
	appInitIr();
	appButtonsInit();
	appInitAdc();

	// Init interrupts and watchdog
	kmCpuInterruptsEnable();
	kmCpuWatchdogEnable(WDTO_500MS);

	//  Show Application Information on the output devices
	appShowInfo();
}

void appLoop(void) {
	kmUsbMidiLoop();
	kmMidiLoop();
	appUsartLoop();
	kmSwtLoop();
	kmIrLoop();
	kmAdcLoop();
	kmCpuWatchdogReset();
}
