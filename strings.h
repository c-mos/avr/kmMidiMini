/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* strings.h
*
*  **Created on**: 3/15/2024 7:13:20 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  strings.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef STRINGS_H_
#define STRINGS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "kmCommon/kmCommon.h"
#include "version.h"

#define KM_STR_APP_NAME_VERSION PSTR(APP_NAME " v" APP_VERSION)
#define KM_STR_APP_COPYRIGHT	PSTR("(c)" APP_YEAR " " APP_AUTHOR)
#define KM_STR_APP_REPO			PSTR(APP_REPO)

#ifdef __cplusplus
}
#endif
#endif /* STRINGS_H_ */

