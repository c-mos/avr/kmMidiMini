/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* version.h
*
*  **Created on**: 3/15/2024 4:54:23 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  version.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef VERSION_H_
#define VERSION_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "kmCommon/kmCommon.h"

#define APP_NAME	"kmMidiMini"
#define APP_VERSION	"0.3.0.0"
#define APP_YEAR	"2024"
#define APP_AUTHOR	"Krzysztof Moskwa"
#define APP_REPO	"https://gitlab.com/c-mos/avr/kmMidiMini"
#define APP_SETTINGS_VER	0

// Hardware type
//#define HW_PCB_MM_V09d
#define HW_PCB_MM_V100

// V-USB vendor, name and serial
#define KM_USB_CFG_VENDOR_NAME     'c', '-', 'm', 'o', 's'
#define KM_USB_CFG_VENDOR_NAME_LEN 5

#define KM_USB_CFG_DEVICE_NAME     'k', 'm', 'M', 'i', 'd', 'i', 'M', 'i', 'n', 'i'
#define KM_USB_CFG_DEVICE_NAME_LEN 10

#define KM_USB_CFG_SERIAL_NUMBER   '0', ',', '2', '.', '0', '.', '0'
#define KM_USB_CFG_SERIAL_NUMBER_LEN   7

#ifdef __cplusplus
}
#endif
#endif /* VERSION_H_ */

