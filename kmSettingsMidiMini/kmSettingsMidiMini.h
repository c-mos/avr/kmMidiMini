/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* @file kmSettingsMidiMini.c
* @brief MIDI settings and control management for kmMidiMini application.
*
* @details
* This file implements configuration and control functions for the kmMidiMini
* application, including handling of MIDI modes, high-resolution MIDI control,
* Force Sensing Linear Potentiometer (FSLP) input, and parameter storage.

* - Implements support for multiple operating modes and profiles.
* - Provides MIDI mode and high-resolution control configuration.
* - Manages EEPROM storage for settings persistence.
*
*  **Created on**: 3/17/2024 1:42:29 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  kmSettingsMidiMini.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KMSETTINGSMIDIMINI_H_
#define KMSETTINGSMIDIMINI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>

#include "../kmCommon/kmCommon.h"

#include "../kmMidi/kmMidi.h"
#include "../kmMidi/kmMidiEnums.h"
#include "../kmAdc/kmAdc.h"
#include "../kmAdc/kmDetector.h"

#define KM_MIDI_CTRL_VOID_DATA 0

typedef struct {
	// Defines main message type
	kmMidiVoiceMessagesType voiceMessageType;
	/// Defines controller message type
	kmMidiCtrlType controllerType;
	/// When TRUE, given controller sends High Res (14-bit) values (applicable for 0-31 controllers)
	bool hiresMode;
	/// Defines channel number, or -1 = use global channel
	int8_t channel;
	/// Default
	uint16_t defaultData;
} kmSettingsMidiController;


typedef enum {
	/// Application is disabled, and no midi messages are sent
	appDisabled = 0x00u,
	/// Using four knobs to send 4 control change messages
	app4Knobs,
	/// Force Sensing Linear Potentiometer
	appFSLP,
	/// Simulation of Universal Controller via IR commands
	appUniversalController
} kmSettingsMidiAppSetup;

/**
* @brief Initializes settings.
* Following definitions to be set in config.h file @n
* #define \b KMMC_MAGIC string that defines module and version allowing to preserve and update settings between versions (e.g. "KMSG100")@n
* #define \b KMMC_MAGIC_LENGTH number of bytes in magic string defined in KMSG_MAGIC (e.g. 8 in above example last byte is '\0' so it's 8 bytes)@n
* NOTE: To preserve EEPROM settings make sure EESAVE fuse bit is correctly defined (EESAVE = 0)@n
* In this version once EEPROM is erased or has different magic value than defined in KMSG_MAGIC then values are overwritten with default presets 1 to 4
*/
void kmSettingsMidiCtrlInit(void);

/**
 * @brief Stores the current MIDI controller settings to EEPROM.
 */
void kmSettingsMidiCtrlStore(void);


/**
 * @brief Sets the application setup for the MIDI controller.
 *
 * @param setup The new application setup configuration.
 * @return The updated application setup.
 */
kmSettingsMidiAppSetup kmSettingsSetAppSetup(const kmSettingsMidiAppSetup setup);
	
/**
 * @brief Retrieves the current application setup for the MIDI controller.
 *
 * @return The current application setup.
 */
kmSettingsMidiAppSetup kmSettingsGetAppSetup(void);

/**
 * @brief Retrieves the detector configuration for a specific ADC slot.
 *
 * @param slot The slot number for which to retrieve the configuration.
 * @return Pointer to the detector configuration, or NULL if the slot is invalid.
 */
const kmDetectorConfig *kmSettingsGetDetectorConfig(const uint8_t slot);

/**
 * @brief Retrieves the MIDI controller assigned to a detector slot.
 * mn
 * @param slot The slot number for which to retrieve the MIDI controller.
 * @return The MIDI controller ID assigned to the specified slot.
 */
uint16_t kmSettingsGetDetectorMidiCtrl(const uint8_t slot);

/**
 * @brief Retrieves the ADC control data array based on the current application setup.
 *
 * @return Pointer to the array of ADC control data.
 */
const kmAdcControlDataType* kmSettingsGetAdcControlDataArray(void);

/**
 * @brief Retrieves the length of the ADC control data array based on the current application setup.
 *
 * @return The number of elements in the ADC control data array.
 */
uint8_t kmSettingsGetAdcControlDataLen(void);

/**
 * @brief Retrieves the configuration for a specific MIDI controller.
 *
 * @param ctrlNo The controller number to retrieve.
 * @return Pointer to the MIDI controller configuration.
 */
kmSettingsMidiController *kmSettingsMidiCtrlGet(const uint8_t ctrlNo);

/**
 * @brief Sets the operating mode for the MIDI controller.
 *
 * @param mode The mode to set.
 */
void kmSettingsMidiCtrlSetMode(const uint8_t mode);

/**
 * @brief Retrieves the current operating mode of the MIDI controller.
 *
 * @return The current operating mode.
 */
uint8_t kmSettingsMidiCtrlGetMode(void);

/**
 * @brief Changes the MIDI mode by incrementing or decrementing it.
 *
 * @param inc True to increment the mode, false to decrement it.
 * @return The new MIDI mode after the change.
 */
uint8_t kmSettingsMidiCtrlChangeMode(bool inc);

/**
 * @brief Retrieves the LED indication pattern for the current mode or setup.
 *
 * @return A bitmask representing the LED indications.
 */
uint8_t kmSettingsMidiCtrlGetIndication(void);

/**
 * @brief Enables or disables high-resolution MIDI control mode.
 *
 * @param enabled True to enable high-resolution (14-bit) mode, false to disable it (7-bit).
 * @return The new high-resolution mode status.
 */
bool kmSettingsMidiCtrlSetHiRes(const bool enabled);

/**
 * @brief Checks if high-resolution MIDI control mode is enabled.
 *
 * @return True if high-resolution mode is enabled, false otherwise.
 */
bool kmSettingsMidiCtrlGetHiRes(void);

/**
 * @brief Resets settings to the default profile for the MIDI controller settings.
 */
void kmSettingsMidiCtrlInitDefaultProfile(void);

#ifdef __cplusplus
}
#endif
#endif /* KMSETTINGSMIDIMINI_H_ */

