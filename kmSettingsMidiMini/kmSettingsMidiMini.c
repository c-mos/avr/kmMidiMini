/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* kmSettingsMidiMini.c
*
*  Created on: 3/17/2024 1:42:45 PM
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Test Application for kmAvrLibs
*  Copyright (C) 2024  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../kmCommon/kmCommon.h"

#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "kmSettingsMidiMini.h"

#include "../kmMidi/kmMidiEnums.h"
#include "../config.h"

typedef struct {
	bool midiHiResEnabled;
	uint8_t midiMode;
	kmSettingsMidiAppSetup appSetup;
	bool fslpHoldPosition;
	bool fslpPressureEnabled;
} kmMidiCtrlSettings;

static kmSettingsMidiController _midiControllerSetup[KM_MIDI_CTRL_SIZE_OF];

typedef struct {
	uint8_t settingsMagic[0x04];
	uint8_t settingsVersion;
	uint8_t settingsActiveSet;
	kmMidiCtrlSettings settings;
	uint8_t settingsCRC;
} kmMidiCtrlProfile_v01;

static kmMidiCtrlProfile_v01 _kmMidiCtrlProfileEEMEM EEMEM;
static kmMidiCtrlProfile_v01 _kmMidiCtrlProfile;

static const kmSettingsMidiController _kmMidiCtrlDefaultSetup[KM_MODE_SIZE_OF][KM_MIDI_CTRL_SIZE_OF] PROGMEM = {
	// Mode 1
	{
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController5,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController2,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController10,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlPortamentoTime,
			.hiresMode = true,
			.channel = -1
		}
	},
	// Mode 2
	{
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController4,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController3,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController6,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController1,
			.hiresMode = false,
			.channel = -1
		}
	},
	// Mode 3
	{
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlChannelVolume,
			.hiresMode = true,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlPan,
			.hiresMode = true,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlGeneralPurposeController3,
			.hiresMode = true,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlGeneralPurposeController4,
			.hiresMode = true,
			.channel = -1
		}
	},
	// Mode 4
	{
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlEffectsDepth1,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlEffectsDepth4,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlEffectsDepth3,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlEffectsDepth2,
			.hiresMode = false,
			.channel = -1
		}
	},
	// Mode 5
	{
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlEffectControl2,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlEffectControlX,
			.hiresMode = true,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlExpressionController,
			.hiresMode = true,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlModulationWheel,
			.hiresMode = true,
			.channel = -1
		}
	},
	// Mode FSLP
	{
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlGeneralPurposeController7,
			.hiresMode = true,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController2,
			.hiresMode = false,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlGeneralPurposeController7,
			.hiresMode = true,
			.channel = -1
		},
		{
			.voiceMessageType = kmMidiControlChange,
			.controllerType = kmMidiCtrlSoundController2,
			.hiresMode = false,
			.channel = -1
		}
	}
};

static const kmAdcControlDataType adcControlDataFLSP[] PROGMEM = {\
	  {KM_FSLP_ADC_CHANNEL_V0, KMC_ALL_BITS_CLEARED} \
	, {KM_FSLP_ADC_CHANNEL_V2, KMC_ALL_BITS_CLEARED} \
	, {KM_FSLP_ADC_CHANNEL_V1, KMC_ALL_BITS_CLEARED} \
	, {KM_FSLP_ADC_CHANNEL_V0, KMC_ALL_BITS_CLEARED} \
};

static const kmAdcControlDataType adcControlData4Knobs[] PROGMEM = {\
	  {KM_ADC_CHANNEL_00, KMC_ALL_BITS_CLEARED} \
	, {KM_ADC_CHANNEL_01, KMC_ALL_BITS_CLEARED} \
	, {KM_ADC_CHANNEL_02, KMC_ALL_BITS_CLEARED} \
	, {KM_ADC_CHANNEL_03, KMC_ALL_BITS_CLEARED} \
};

#define KM_SETTINGS_CONTROL_DATA_LEN	KM_ADC_NUMBER_OF_SLOTS

/**
@brief Detector configuration for disabled detection.
*/
static const kmDetectorConfig settingsDetectDisabled = {
	.detectorType			= kmDetectNone
};

const kmDetectorConfig settingsDetectPotentiometerTo14Bits = {
	.detectorType			= kmDetectDelta,
	.delta					= 0x0040u,
	.rightShiftScalingFactor = 0x07u,
	.resultRightBitShift	= 0x02u,
};

const kmDetectorConfig settingsDetectPotentiometerTo14BitsPressure = {
	.detectorType			= kmDetectDelta,
	.delta					= 0x0040u,
	.rightShiftScalingFactor = 0x01u,
	.resultRightBitShift	= 0x02u,
};

const kmDetectorConfig settingsDetectPotentiometerTo14BitsInv = {
	.detectorType			= kmDetectDelta,
	.delta					= 0x0040u,
	.rightShiftScalingFactor = 0x07u,
	.resultRightBitShift	= 0x02u,
	.resultSubstractFrom	= 0xffffu,
};

uint8_t _calculateCRC8(const uint8_t* data, const size_t length) {
	uint8_t crc = KMC_UNUSED_8_BIT;  // Non-zero initial seed
	const uint8_t polynomial = 0x07;

	for (size_t i = 0; i < length; i++) {
		crc ^= data[i];
		for (uint8_t j = 0; j < 8; j++) {
			if (crc & 0x80) {
				crc = (crc << 1) ^ polynomial;
				} else {
				crc <<= 1;
			}
		}
	}
	return crc;
}

kmSettingsMidiAppSetup kmSettingsSetAppSetup(const kmSettingsMidiAppSetup setup) {
	_kmMidiCtrlProfile.settings.appSetup = setup;
	return _kmMidiCtrlProfile.settings.appSetup;
}

kmSettingsMidiAppSetup kmSettingsGetAppSetup(void) {
	return _kmMidiCtrlProfile.settings.appSetup;
}

const kmDetectorConfig *kmSettingsGetDetectorConfig(const uint8_t slot) {
	if (slot >= KM_ADC_NUMBER_OF_DETECTORS) {
		return NULL;
	}
	
	switch (kmSettingsGetAppSetup()) {
		case app4Knobs : {
			return &settingsDetectPotentiometerTo14Bits;
		}
		case appFSLP : {
			switch (slot) {
				case KM_FSLP_SLOT_0 : {
					return _kmMidiCtrlProfile.settings.fslpHoldPosition ? &settingsDetectPotentiometerTo14BitsInv : &settingsDetectDisabled;
					break;
				}
				case KM_FSLP_SLOT_1 : {
					return _kmMidiCtrlProfile.settings.fslpHoldPosition ? (_kmMidiCtrlProfile.settings.fslpPressureEnabled ? &settingsDetectPotentiometerTo14BitsPressure : &settingsDetectDisabled) : &settingsDetectDisabled;
					break;
				}
				case KM_FSLP_SLOT_2 : {
					return _kmMidiCtrlProfile.settings.fslpHoldPosition ? &settingsDetectDisabled : &settingsDetectPotentiometerTo14BitsInv;
					break;
				}
				case KM_FSLP_SLOT_3 : {
					return _kmMidiCtrlProfile.settings.fslpHoldPosition ? &settingsDetectDisabled : (_kmMidiCtrlProfile.settings.fslpPressureEnabled ? &settingsDetectPotentiometerTo14BitsPressure : &settingsDetectDisabled);
					break;
				}
				default: {} // intentionally
			}
		}
		default: {
			return &settingsDetectDisabled;
		}
	}
}

uint16_t kmSettingsGetDetectorMidiCtrl(const uint8_t slot) {
	return slot;
}

const kmAdcControlDataType *kmSettingsGetAdcControlDataArray(void) {
	return (_kmMidiCtrlProfile.settings.appSetup == appFSLP ? adcControlDataFLSP : adcControlData4Knobs);
}

uint8_t kmSettingsGetAdcControlDataLen(void) {
	return KM_SETTINGS_CONTROL_DATA_LEN;
}

kmSettingsMidiController *kmSettingsMidiCtrlGet(const uint8_t ctrlNo) {
	return &_midiControllerSetup[ctrlNo];
}

void kmSettingsMidiCtrlSetMode(const uint8_t mode) {
	if (mode >= KM_MODE_SIZE_OF) return;

	const kmSettingsMidiController* src = &_kmMidiCtrlDefaultSetup[mode][0];

	memcpy_P(_midiControllerSetup, src, sizeof(kmSettingsMidiController) * KM_MIDI_CTRL_SIZE_OF);
	_kmMidiCtrlProfile.settings.midiMode = mode;

	// disable hires after setting specific mode for all controllers, when it is not enabled
	if (!kmSettingsMidiCtrlGetHiRes()) {
		for (uint8_t i = KMC_UNSIGNED_ZERO; i < KM_MIDI_CTRL_SIZE_OF; i++) {
			_midiControllerSetup[i].hiresMode = false;			
		}
	}
}

uint8_t kmSettingsMidiCtrlGetMode(void) {
	if (_kmMidiCtrlProfile.settings.appSetup == appFSLP) {
		return KM_MODE_FSLP;
	}
	return _kmMidiCtrlProfile.settings.midiMode;
}

uint8_t kmSettingsMidiCtrlChangeMode(bool inc) {
	switch (_kmMidiCtrlProfile.settings.appSetup) {
		case appFSLP : {
			if (inc == false) { // button 1
				_kmMidiCtrlProfile.settings.fslpHoldPosition = !_kmMidiCtrlProfile.settings.fslpHoldPosition;
			}
			if (inc == true) { // button 2
				_kmMidiCtrlProfile.settings.fslpPressureEnabled = !_kmMidiCtrlProfile.settings.fslpPressureEnabled;
			}
			kmSettingsMidiCtrlSetMode(KM_MODE_FSLP);
			return KMC_UNSIGNED_ZERO; // no break needed
		}
		default : {
			if (inc == true && _kmMidiCtrlProfile.settings.midiMode < KM_MODE_MAX) _kmMidiCtrlProfile.settings.midiMode++;
			if (inc == false && _kmMidiCtrlProfile.settings.midiMode > KM_MODE_MIN) _kmMidiCtrlProfile.settings.midiMode--;
			kmSettingsMidiCtrlSetMode(_kmMidiCtrlProfile.settings.midiMode);
			return _kmMidiCtrlProfile.settings.midiMode; // no break needed
		}
	}
}

uint8_t kmSettingsMidiCtrlGetIndication(void) {
	static const uint8_t _modeToIndicationMap[] PROGMEM = KM_MODE_TO_INDICATION_MAP;
	switch (_kmMidiCtrlProfile.settings.appSetup) {
		case appFSLP : {
			return (_kmMidiCtrlProfile.settings.fslpHoldPosition ? _BV(KM_MM_LED_2) : KMC_UNSIGNED_ZERO) | (_kmMidiCtrlProfile.settings.fslpPressureEnabled ? _BV(KM_MM_LED_1) : KMC_UNSIGNED_ZERO) | _BV(KM_MM_LED_STATUS);
		}
		default : {
			return pgm_read_byte(&_modeToIndicationMap[_kmMidiCtrlProfile.settings.midiMode]);
		}
	}
}

bool kmSettingsMidiCtrlSetHiRes(const bool enabled) {
	_kmMidiCtrlProfile.settings.midiHiResEnabled = enabled;
	kmSettingsMidiCtrlSetMode(kmSettingsMidiCtrlGetMode());
	return enabled;
}

bool kmSettingsMidiCtrlGetHiRes(void) {
	return _kmMidiCtrlProfile.settings.midiHiResEnabled;
}

void kmSettingsMidiCtrlInitDefaultProfile(void) {
	_kmMidiCtrlProfile = (kmMidiCtrlProfile_v01){
		.settingsMagic = { 'k', 'm', 'M', 'D' },
		.settingsVersion = APP_SETTINGS_VER,
		.settings = {
			.midiHiResEnabled = false,
			.midiMode = KMC_UNSIGNED_ZERO,
			.appSetup = app4Knobs,
			.fslpHoldPosition = false,
			.fslpPressureEnabled = false
		},
		.settingsCRC = KMC_UNSIGNED_ZERO
	};
}

void kmSettingsMidiCtrlInit(void) {
	eeprom_read_block(&_kmMidiCtrlProfile, &_kmMidiCtrlProfileEEMEM, sizeof(kmMidiCtrlProfile_v01));

	uint8_t crc = _kmMidiCtrlProfile.settingsCRC;
	_kmMidiCtrlProfile.settingsCRC = KMC_UNSIGNED_ZERO;
	if (crc != _calculateCRC8((uint8_t*)&_kmMidiCtrlProfile, sizeof(kmMidiCtrlProfile_v01))) {
		kmSettingsMidiCtrlInitDefaultProfile();
	}
	
	kmSettingsMidiCtrlSetHiRes(kmSettingsMidiCtrlGetHiRes());
	kmSettingsMidiCtrlSetMode(kmSettingsMidiCtrlGetMode());
	
}

void kmSettingsMidiCtrlStore(void) {
	uint8_t crc = _calculateCRC8((uint8_t*)&_kmMidiCtrlProfile, sizeof(kmMidiCtrlProfile_v01));
	_kmMidiCtrlProfile.settingsCRC = crc;
	 eeprom_update_block(&_kmMidiCtrlProfile, &_kmMidiCtrlProfileEEMEM, sizeof(kmMidiCtrlProfile_v01));
}
