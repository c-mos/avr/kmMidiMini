/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* kmSettingsMidiMiniDefaultConfig.h
*
*  **Created on**: 9/26/2024 9:38:51 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  kmSettingsMidiMiniDefaultConfig.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KMSETTINGSMIDIMINIDEFAULTCONFIG_H_
#define KMSETTINGSMIDIMINIDEFAULTCONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../kmCommon/kmCommon.h"

#define KM_MIDI_CTRL_0 0x00u
#define KM_MIDI_CTRL_1 0x01u
#define KM_MIDI_CTRL_2 0x02u
#define KM_MIDI_CTRL_3 0x03u

#define KM_MIDI_CTRL_SIZE_OF			0x04u

#ifdef __cplusplus
}
#endif
#endif /* KMSETTINGSMIDIMINIDEFAULTCONFIG_H_ */

