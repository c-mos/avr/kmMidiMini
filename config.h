/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* config.h
*
*  **Created on**: 4/13/2024 7:21:16 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  config.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef CONFIG_H_
#define CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "kmCommon/kmCommon.h"
//#define KM_MIDI_MINI_SETUP_USB_HID
#define KM_MIDI_MINI_SETUP_USB_MIDI
#include "kmMidiMiniSetup/kmMidiMiniSetup.h"

// Application Settings
#define KM_MODE_SIZE_OF 0x06u
#define KM_MODE_MIN 0x00u
#define KM_MODE_MAX 0x04u
#define KM_MODE_FSLP 0x05u
#define KM_MODE_CHANGE_SHOW 0x00u
#define KM_MODE_CHANGE_BLINKS 0x01u
#define KM_MODE_SAVE_BLINKS 0x02u

// Log
///#define KM_DEBUG_DISABLE
#define KM_LOG_DISABLE
//#define KM_LOG_ADC_EVERY 20
//#define KM_LOG_ADC_DETECTOR
//#define KM_FUNC_INPUTS_CHECK
//#define KM_NO_ADC_DETECTOR

#ifndef KM_LOG_DISABLE
#include "kmLog/kmLogDefaultConfig.h"
#endif

// Debug/Indication Config
#define KM_DEBUG_INVERTED_LOGIC
#define KM_DEBUG_DDR				KM_MM_LED_DDR
#define KM_DEBUG_PORT				KM_MM_LED_PORT
#define KM_DEBUG_PIN_0				KM_MM_LED_STATUS
#define KM_DEBUG_PIN_1				KM_MM_LED_1
#define KM_DEBUG_PIN_2				KM_MM_LED_2

// Software Timer Config
#define SWT_SIZE_OF					10
#define KM_SWT_SLOT_USB				KM_SWT_TIMER_0
#define KM_USB_SWT_SLOT				KM_SWT_TIMER_1
#define KM_SWT_SLOT_STATUS_LED		KM_SWT_TIMER_2
#define KM_SWT_SLOT_IR_LED			KM_SWT_TIMER_3
#define KM_SWT_SLOT_MIDI_LED		KM_SWT_TIMER_4
#define KM_SWT_SLOT_MODE_LED		KM_SWT_TIMER_5
#define KM_SWT_SLOT_BUTTONS			KM_SWT_TIMER_6
#define KM_STATUS_LED_TIME_ON		10 //ms
#define KM_STATUS_LED_TIME_OFF		410 //ms
#define KM_IR_LED_TIME_ON			100//ms
#define KM_ADC_LED_TIME_ON			10//ms
#define KM_MIDI_LED_TIME_ON			10//ms
#define APP_MODE_LED_TIME_ON		750 //ms
#define APP_MODE_LED_TIME_OFF		500 //ms

// Buttons
#define KM_BUTTON_PORT				KM_MM_BTN_PORT
#define KM_BUTTON_DDR				KM_MM_BTN_DDR
#define KM_BUTTON_PORT_IN			KM_MM_BTN_IN
#define KM_BUTTON_MASK				(_BV(KM_MM_BTN1) | _BV(KM_MM_BTN2))
#define KM_BUTTON_CHECK_INTERVAL	KM_SWT_INTERVAL_10MS
#define KM_BUTTON_REPEAT_SUPPORT_NO 0x04u
#define KM_BUTTON_REPEAT_DELAY 		75u
#define KM_BUTTON_REPEAT_INTERVAL	KM_BUTTON_REPEAT_DELAY

// USART / Terminal Config
#define KM_USART_RX_BUFFER_SIZE		8
#define KM_USART_TX_BUFFER_SIZE		32
#define KM_USART_AVAILABLE_VIRTUAL	2
#define KM_USART_DEBUG_SPEED		KM_USART_250000

// IR
#define KM_IR_DEFAULT_IDDLE_TIME	5000

// Timer0
#include "kmTimer0/kmTimer0DefaultConfig.h"

// ADC
#define KM_ADC_USE_FSLP
//#define KM_ADC_FSLP_INVERT

#define KM_ADC_MAIN_SAMPLING_PERIOD		500u // 250 is minimum for stable behavior with V-USB (16MHz main clock)
#define KM_ADC_NUMBER_OF_SLOTS			0x04
#define KM_ADC_NUMBER_OF_DETECTORS		KM_ADC_NUMBER_OF_SLOTS
#define KM_ADC_AVERAGER_RANGE			0x04u
#define KM_ADC_DEFAULT_REF				KM_ADC_REF_AVCC
#define KM_ADC_SHIFT_BITS_LEFT_TO_NORMALIZE_RESULT 0x06u

#define KM_ADC_FSLP_MIN_FORCE_DETECTED	300u
#define KM_FSLP_DDR						KM_MM_GPIO_DDR
#define KM_FSLP_PORT					KM_MM_GPIO_PORT

#define KM_FSLP_GPIO_0					KM_MM_GPIO_0	// RO
#define KM_FSLP_GPIO_1					KM_MM_GPIO_3	// SL
#ifdef KM_ADC_FSLP_INVERT
#define KM_FSLP_GPIO_2					KM_MM_GPIO_1	// DL1
#define KM_FSLP_GPIO_3					KM_MM_GPIO_2	// DL2
#else
#define KM_FSLP_GPIO_2					KM_MM_GPIO_2	// DL2
#define KM_FSLP_GPIO_3					KM_MM_GPIO_1	// DL1
#endif /* KM_FSLP_INVERT */

//#define KM_FSLP_SLOT_PREP				0x00u
#define KM_FSLP_SLOT_0					0x00u
#define KM_FSLP_SLOT_1					0x01u
#define KM_FSLP_SLOT_2					0x02u
#define KM_FSLP_SLOT_3					0x03u

#define KM_ADC_FSLP_NEUTRAL_VALUE		0x0000u

#define KM_DETECTOR_MIN_CHANGE_DETECTED	0x010u

#define KM_ADC_SLOT_0					0x00u
#define KM_ADC_SLOT_1					0x01u
#define KM_ADC_SLOT_2					0x02u
#define KM_ADC_SLOT_3					0x03u
// Application
#define KM_APP_HID_UPDATE_INTERVAL		10u

// Midi
#include "kmSettingsMidiMini/kmSettingsMidiMiniDefaultConfig.h"
#include "kmMidi/kmMidiDefaultConfig.h"

// V-USB
#ifdef KM_MIDI_MINI_SETUP_USB_HID
/// Enable USB HID support
#define KM_USB_HID_ENABLED

/// Enable USB Mouse support
#define KM_USB_MOUSE_ENABLED

/// Enable USB Keyboard support
#define KM_USB_KEYBOARD_ENABLED

/// Size of the keyboard buffer
#define KM_USB_KB_BUFF_LEN 5

/// Size of the HID commands buffer used in #kmUsbHidSendRaw function
#define KM_USB_HID_BUFFER_SIZE 64

#include "kmUsb/default/kmUsbDefsHidDefault.h"
#include "kmUsb/descriptor/kmUsbDescriptorHidDefault.h"
#include "kmUsb/vendorID/kmUsbGenericVendorHID.h"
#include "kmUsb/default/kmUsbDefsCommonDefault.h"
#include "kmUsb/default/kmUsbNameVendorDefault.h"
#include "kmUsb/default/kmUsbNameDeviceDefault.h"
#include "kmUsb/default/kmUsbDeviceVersionDefault.h"
#endif /* KM_MIDI_MINI_SETUP_USB_HID */

#ifdef KM_MIDI_MINI_SETUP_USB_MIDI
#define KM_USB_MIDI_ENABLED
#include "kmUsb/default/kmUsbNameVendorGenericKM.h" // taken from version.h
#include "kmUsb/default/kmUsbNameDeviceGenericKM.h" // taken from version.h
#include "kmUsb/default/kmUsbNameSerialGenericKM.h" // taken from version.h
#include "kmUsb/default/kmUsbConfigMidiDefault.h"
#define KM_USB_MIDI_BUFFER_SIZE		0x16
#endif /* KM_MIDI_MINI_SETUP_USB_HID */

#include "kmUsb/default/kmUsbPower500mADefault.h"
#include "kmUsb/default/kmUsbClock16MHzDefault.h"

#define USB_CFG_IOPORTNAME		KM_USB_CFG_IOPORTNAME
#define USB_CFG_DMINUS_BIT		KM_USB_CFG_DMINUS_BIT
#define USB_CFG_DPLUS_BIT		KM_USB_CFG_DPLUS_BIT

// Midi
#define KM_MODE_TO_INDICATION_MAP { 0x01u, 0x03u, 0x02u, 0x06u, 0x04u, 0x05u, 0x07u};
#define KM_MODE_INDICATION_MAP_HIRES_OFFSET 0x05u


#ifdef __cplusplus
}
#endif
#endif /* CONFIG_H_ */
